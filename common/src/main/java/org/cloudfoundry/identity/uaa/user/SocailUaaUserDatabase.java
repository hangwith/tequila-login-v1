package org.cloudfoundry.identity.uaa.user;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.hangwith.common.model.ModerationActivity;
import com.hangwith.common.model.friendship.FriendshipActivity.ActivityType;
import com.hangwith.common.model.installation.Installation;
import com.hangwith.common.model.security.User;

public class SocailUaaUserDatabase implements UaaUserDatabase {
	
	@Autowired
	MongoTemplate mongoTemplate;
	

    private final Log logger = LogFactory.getLog(getClass());

	
	public SocailUaaUserDatabase(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}



	@Override
	public UaaUser retrieveUserByName(String username, String origin)
			throws UsernameNotFoundException {
		if (StringUtils.isBlank(username)) {
			logger.warn("Username is blank");
			return null;
		}
		User user =  trySocailLogin(username);
		if (user==null){
			user = mongoTemplate.findOne(query(where("s_username").is(username.toLowerCase(Locale.US))), User.class);			
		}
		if (user == null) {			
			logger.info("Unable to find user: " + username);
			return null;
		}
		if (user.getActive() != null && !user.getActive()) {
			if (checkSuspended(user.getObjectId())){
				logger.info("The user: " + username +" is banned or suspended") ;
				return null;
			}
		}
		List<GrantedAuthority> authorities= getAuthorities(user);
		Date createDate = user.getCreatedAt() != null ? user.getCreatedAt()
				.toDate() : new Date();
		Date updatedDate = user.getUpdatedAt() != null ? user.getUpdatedAt()
				.toDate() : new Date();
	
		return new UaaUser(user.getObjectId(), user.getS_username(),
							user.getBcryptPassword(), user.getEmail(),
							authorities, user.getF_username(),
				user.getS_name(), createDate, updatedDate, user.getPassword(), user.getFacebook_id());
	
	}

	/**
	 * Convert comma delimited authorities
	 * @param user
	 * @return
	 */
	private List<GrantedAuthority> getAuthorities(User user) {
		ArrayList<GrantedAuthority> values = new ArrayList<GrantedAuthority>();
		if (StringUtils.isNotBlank(user.getAuthorities())){
			
			String[] split = StringUtils.split(user.getAuthorities(),",");
			for (int i = 0; i < split.length; i++) {
				String authority = split[i];
				values.add(UaaAuthority.authority(authority.toString()));
			}
			
		}
		 if (!values.contains(UaaAuthority.HANGWITH_USER)) {
	            values.add(UaaAuthority.HANGWITH_USER);
	     }
		 return values;
	}



	private User trySocailLogin(String token) {
		
		if (token.length()>200){
			String[] split = StringUtils.split(token,":");
			if (split.length!=2){
				return null;
			}
			String facebookId = split[0];
			String fbToken = split[1];
			if (UserUtils.INSTANCE.fbTokenValid(facebookId, fbToken)){
				User user = mongoTemplate.findOne(query(where("facebook_id").is(facebookId)), User.class);
				// this will be used in AuthzAuthenticationManager to bypass the passwordMatch
				user.setPassword(facebookId);					
				return user;		
			}else{
				logger.warn("Facebook token is not valid: "+token);
			}
					
		}
		return null;
	}



	@Override
	public UaaUser retrieveUserById(String id) throws UsernameNotFoundException {
		User user = mongoTemplate.findOne(query(where("objectId").is(id)), User.class );
        if (user==null){
           return null;
        }
        Date createDate = user.getCreatedAt()!=null?user.getCreatedAt().toDate(): new Date();
        Date updatedDate = user.getUpdatedAt()!=null?user.getUpdatedAt().toDate():createDate;
        List<GrantedAuthority> authorities= getAuthorities(user);
        return new UaaUser(user.getObjectId(), user.getS_username(), user.getBcryptPassword(),  user.getEmail(),
        		authorities,
                user.getF_username(), user.getS_name(), createDate,  updatedDate,
                null, null) ;	
	}

	public  boolean  checkSuspended(final String userId){
		boolean suspended = false;
		Object id  = userId.length() > 10 ? new ObjectId(userId) : userId;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("user.$id").is(id));
		
		List<ModerationActivity> moderationActivities = mongoTemplate.find(query, ModerationActivity.class);
		if (moderationActivities == null || moderationActivities.isEmpty()){
			return false;
		}
		DateTime now = DateTime.now(DateTimeZone.UTC);
		for (ModerationActivity activity : moderationActivities) {			
			if (activity.getExpirationDate()!=null && now.isAfter(activity.getExpirationDate()) && ActivityType.SUSPEND.toString().equalsIgnoreCase(activity.getType())){
				User dbUser = mongoTemplate.findById(userId, User.class);
				activity.setUser(dbUser);
				if (activity.getBlockedDevice()!=null){
					activity.setBlockedDevice( mongoTemplate.findById(activity.getBlockedDevice().getObjectId(), Installation.class));
				}
				mongoTemplate.save(activity, "ModerationActivityArchive");
				mongoTemplate.remove(query(new Criteria().andOperator(Criteria.where("user.$id").is(id), Criteria.where("type").is("suspend"))), ModerationActivity.class);
				mongoTemplate.updateFirst(query(Criteria.where("_id").is(id)), new Update().set("active", true).set("updatedAt", now), User.class);
				logger.info("Removed suspend flag for user and made it active:"+userId);
			}else{
				suspended = true;
			}
			logger.warn(String.format("ModerationActivity: User %s %s",userId,activity.getType()));	
		}
		return suspended;
		
	}
}
