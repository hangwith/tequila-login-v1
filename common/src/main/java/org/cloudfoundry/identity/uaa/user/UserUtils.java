package org.cloudfoundry.identity.uaa.user;

import org.apache.commons.lang3.StringUtils;
import org.cloudfoundry.identity.uaa.client.SocialClientUserDetails;
import org.cloudfoundry.identity.uaa.client.SocialClientUserDetailsSource;
import org.cloudfoundry.identity.uaa.oauth.RemoteUserAuthentication;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
//import com.hangwith.common.model.security.User;

import com.hangwith.common.model.HangwithException;

/**
 * Common utility calls to get user information
 * @author rshahbazi
 * @since 07/02/2014
 *
 */
public enum UserUtils {
	
	//Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	INSTANCE;
	
	private OAuth2ClientContext context = new DefaultOAuth2ClientContext();
	private SocialClientUserDetailsSource filter = new SocialClientUserDetailsSource();
	
	public boolean hasRole(final String  role){
		Object principal = SecurityContextHolder.getContext().getAuthentication();
		
		if (principal instanceof UsernamePasswordAuthenticationToken ){
			UsernamePasswordAuthenticationToken p = (UsernamePasswordAuthenticationToken) principal;
			User user = (User)p.getPrincipal();
			if (user!=null && user.getAuthorities()!=null){
				for (GrantedAuthority authority : user.getAuthorities()) {
					if (role.equalsIgnoreCase(authority.getAuthority())){
						return true;
					}
					
				}
			}
		}else if (principal instanceof OAuth2Authentication){
			OAuth2Authentication p = (OAuth2Authentication) principal;
			if (p!=null && p.getAuthorities()!=null){
				for (GrantedAuthority authority : p.getAuthorities()) {
					if (role.equalsIgnoreCase(authority.getAuthority())){
						return true;
					}
					
				}
			}
		}
			
		return false;
	}
	/*
	 * Highest level of privilege
	 */
	public boolean isSuper(){
		 return hasRole("hangwith.super") ;
	}
	public boolean isAdmin(){
		return hasRole("ROLE_ADMIN") || hasRole("hangwith.admin")  || isSuper();
	}
	public boolean isModerator(){
		return  hasRole("hangwith.moderator")  || isAdmin();
	}
	/**
	 * Allow admin user to override the principal
	 * @param userId
	 * @return
	 */
	public com.hangwith.common.model.security.User getUser(String userId){
		if (!isAdmin() || StringUtils.isBlank(userId)){
			return  getUser();
		}
		com.hangwith.common.model.security.User user = new  com.hangwith.common.model.security.User();
		user.setObjectId(userId);
		return user;
	}
	public com.hangwith.common.model.security.User getUser(){
		Object principal = SecurityContextHolder.getContext().getAuthentication();
		
		if (principal instanceof UsernamePasswordAuthenticationToken ){
			
			com.hangwith.common.model.security.User user = new  com.hangwith.common.model.security.User();
			UsernamePasswordAuthenticationToken p = (UsernamePasswordAuthenticationToken) principal;
			User ru =  (User) p.getPrincipal();
			user.setUsername(ru.getUsername());
			user.setObjectId(ru.getUsername());
			return user;
		}else 
		if (principal instanceof OAuth2Authentication){
			OAuth2Authentication p = (OAuth2Authentication) principal;
			if (p.getUserAuthentication() !=null && p.getUserAuthentication() instanceof RemoteUserAuthentication){
				RemoteUserAuthentication ru = (RemoteUserAuthentication)p.getUserAuthentication(); 
				com.hangwith.common.model.security.User user = new  com.hangwith.common.model.security.User();
				user.setUsername(ru.getUsername());
				user.setObjectId(ru.getId());
				user.setEmail(ru.getEmail());
				if (p.getAuthorities()!=null){				
					 	StringBuilder sb = new StringBuilder();
			            int i = 0;
			            for (GrantedAuthority authority: p.getAuthorities()) {
			                if (i++ > 0) {
			                    sb.append(",");
			                }
			                sb.append(authority);
			            }
			            user.setAuthorities(sb.toString());
				}
				
				return user;
			}						
		}
		return null;
	}
	
	public boolean fbTokenValid(String facebookId,String token) {
		
		if (StringUtils.isNotBlank( token)){			
			AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
	        resource.setAuthenticationScheme(AuthenticationScheme.query);
	        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, context);
	        context.setAccessToken(new DefaultOAuth2AccessToken(token));
	        filter.setRestTemplate(restTemplate);
	        filter.setUserInfoUrl("https://graph.facebook.com/me");
	        filter.afterPropertiesSet();
	        SocialClientUserDetails priciple = null;
	        try{
	        	 priciple = (SocialClientUserDetails) filter.getPrincipal();	        	
	        }catch(Exception e){
	        	throw new HangwithException(403,"signup.fb.invalidToken", "The provided Facebook token is not valid. "+e.getLocalizedMessage() );
	        }
			
			if (priciple==null || priciple.getExternalId()==null){
				return false;
			}
			if ( ! priciple.getExternalId().equals(facebookId)){
				return false;
			}
			return true;	
		}
		return false;
	}
}
