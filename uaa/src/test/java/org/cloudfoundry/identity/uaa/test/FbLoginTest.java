package org.cloudfoundry.identity.uaa.test;

import org.cloudfoundry.identity.uaa.client.SocialClientUserDetailsSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hangwith.login.config.SocailLoginConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SocailLoginConfig.class)
public class FbLoginTest {
	
	@Autowired
	SocialClientUserDetailsSource fbClientUserDetailsSource;

}
