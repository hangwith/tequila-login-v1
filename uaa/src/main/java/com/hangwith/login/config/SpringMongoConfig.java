
package com.hangwith.login.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoTypeMapper;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

@Configuration
@EnableMongoRepositories(value = "com.hangwith.login")
public class SpringMongoConfig extends AbstractMongoConfiguration {
	@Value("${mongo-server}")
	String mongoServer;
	
	@Override
	public String getDatabaseName() {
		return "hangwith";
	}

	@Override
	@Bean
	public Mongo mongo() throws Exception {
		//return new Mongo(mongoServer);
		String[] servers = StringUtils.split(mongoServer, ",");
		if (servers.length>1){
			final List<ServerAddress> serverList = new ArrayList<ServerAddress> ();
			for (String server : servers) {
				serverList.add(new ServerAddress(server));
			}
			return new MongoClient(serverList);
		}
		return new MongoClient(mongoServer);
	
	}
 
	public @Bean MongoTemplate mongoTemplate() throws Exception {
		SimpleMongoDbFactory factory = getMongoFactory();	
//		GenericConversionService conversionService = new DefaultConversionService();
//		
//		List<Object> converters = new ArrayList<Object>();
//		//MappingMongoConverter  converter = getMappingMongoConverter(); 
//
//		MappingMongoConverter converter = 
//				new MappingMongoConverter(mongoDbFactory(), new MongoMappingContext());
//			converter.setTypeMapper(new DefaultMongoTypeMapper(null));
//		 
//		 
//		
//		converters.add(StringToDateTimeConverter.INSTANCE);
//		CustomConversions conversions = new CustomConversions(converters);
//		converter.setCustomConversions(conversions );
//		
//		
		
		
		MongoTemplate mongoTemplate = new MongoTemplate(factory, mongoConverter() );
		
		//HealthChecks.register(new ChauffeurHealthCheck(mongoTemplate));
		return mongoTemplate;
	}
	SimpleMongoDbFactory getMongoFactory() throws Exception{
		SimpleMongoDbFactory factory = new SimpleMongoDbFactory(mongo(), getDatabaseName());
		return factory;
	}
	MappingMongoConverter getMappingMongoConverter() throws Exception{
		SimpleMongoDbFactory factory =getMongoFactory();
		MappingMongoConverter converter = new MappingMongoConverter(factory, new MongoMappingContext());
		return converter;
	}
	
	@Bean
	public GridFsTemplate gridFsTemplate() throws Exception{
		return new GridFsTemplate(getMongoFactory(), getMappingMongoConverter());
	}
	
	@Bean
    public MongoTypeMapper mongoTypeMapper() {
        return new DefaultMongoTypeMapper(null);
    }
 
    @Bean
    public MongoMappingContext mongoMappingContext() {
        return new MongoMappingContext();
    }
 
    @Bean
    public MappingMongoConverter mongoConverter() throws Exception {
        MappingMongoConverter converter = new MappingMongoConverter(getMongoFactory(), mongoMappingContext());
        List<Object> converters = new ArrayList<Object>();
		converters.add(StringToDateTimeConverter.INSTANCE);
        converters.add(DateToDateTimeConverter.INSTANCE);
		CustomConversions conversions =new CustomConversions(converters );
		converter.setCustomConversions(conversions);
        converter.setTypeMapper(mongoTypeMapper());
        return converter;
    }
	
	public static enum StringToDateTimeConverter implements Converter<String, DateTime> {

		INSTANCE;
		
		public DateTime convert(String source) {
			return  new DateTime(source,DateTimeZone.UTC);
		}
	}
    public static enum DateToDateTimeConverter implements Converter<Date, DateTime> {

        INSTANCE;

        public DateTime convert(Date source) {
            return  new DateTime(source,DateTimeZone.UTC);
        }
    }
    @Override
    protected String getMappingBasePackage() {
        return "com.hangwith.friendship";
    }
}
