
package com.hangwith.login.config;


import org.cloudfoundry.identity.uaa.client.SocialClientUserDetailsSource;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan( basePackages = "com.hangwith.login",excludeFilters = { @ComponentScan.Filter( Configuration.class ) } )
@ImportResource({"classpath:spring-servlet.xml","classpath:spring/integration-login.xml"})
public class ApplicationConfig{
	
	private OAuth2ClientContext context = new DefaultOAuth2ClientContext();
	@Bean
	public static PropertyPlaceholderConfigurer properties(){
		final PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
		final Resource[] resources = new ClassPathResource[ ] {  new ClassPathResource("/login.properties") };
		ppc.setLocations( resources );
		ppc.setIgnoreUnresolvablePlaceholders( true );
		return ppc;
	}
	
	
	/*@Bean
	RestTemplate restTemplate(){
		AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
	    resource.setAuthenticationScheme(AuthenticationScheme.query);
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, context);

	}*/
	
	@Bean
	SocialClientUserDetailsSource fbClientUserDetailsSource(){		
		AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
	    resource.setAuthenticationScheme(AuthenticationScheme.query);
	    OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, context);	        
		SocialClientUserDetailsSource filter = new SocialClientUserDetailsSource();
		filter.setRestTemplate(restTemplate);
		filter.setUserInfoUrl("https://graph.facebook.com/me");
		filter.afterPropertiesSet();
		return filter;
	}
	@Bean
    PasswordEncoder passwordEncoder(){
        return  new BCryptPasswordEncoder();
    }
}
