package com.hangwith.login.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.hangwith.common.model.BaseModel;

@Configuration
public class RedisConfig  {
	private final Log logger = LogFactory.getLog(getClass());
	@Value("${redis.host}")
	String redisHost;

	@Value("${redis.port}")
	Integer redisPort;

	@Value("${redis.password}")
	String redisPassword;
	
	
	

	@Value("${redis.master}")
	private String master =  "localhost";
	
	@Value("${redis.sentinel1}")
	private String sentinel1 =  "localhost";
	
	@Value("${redis.sentinel2}")
	private String sentinel2 =  "localhost";
	
	@Value("${redis.sentinel1.port}")
	private Integer sentinel1Port =  26379;
	
	@Value("${redis.sentinel2.port}")
	private Integer sentinel2Port =  26379;
	
	
	@Bean
    public RedisConnectionFactory jedisConnectionFactory() {
		
		if (master==null || "localhost".equals(master)){
			return redisConnectionFactory();
		}
        return new JedisConnectionFactory(sentinelConfig());
    }

	@Bean
	public RedisConnectionFactory redisConnectionFactory() {
		JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
		jedisConnectionFactory.setHostName(redisHost);
		jedisConnectionFactory.setPort(redisPort);
		jedisConnectionFactory.setPassword(redisPassword);
		jedisConnectionFactory.setUsePool(true);
		return jedisConnectionFactory;
	}

	@Bean
	public RedisTemplate<String, BaseModel> redisTemplate() {
		RedisTemplate<String, BaseModel> template = new RedisTemplate<>();
		template.setConnectionFactory(jedisConnectionFactory());
		template.setKeySerializer(new StringRedisSerializer());
		template.setValueSerializer(new Jackson2JsonRedisSerializer<>(BaseModel.class));
		template.setHashKeySerializer(new StringRedisSerializer());
		template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(BaseModel.class));
		return template;
	}

	@Bean
    public RedisSentinelConfiguration sentinelConfig() {
        return redisSentinelConfiguration();
    }
	
	public RedisSentinelConfiguration redisSentinelConfiguration() {
        

        logger.debug("REDIS_MASTER-->" + master);

        final RedisSentinelConfiguration SENTINEL_CONFIG = new RedisSentinelConfiguration().master(master);
       
        if (sentinel1!=null){
        	SENTINEL_CONFIG.addSentinel(new RedisNode(sentinel1, sentinel1Port));
        }
        if (sentinel2!=null){
        	SENTINEL_CONFIG.addSentinel(new RedisNode(sentinel2, sentinel2Port));
        }
                
        return SENTINEL_CONFIG;
    }
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	
}
