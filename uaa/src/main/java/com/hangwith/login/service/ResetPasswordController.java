package com.hangwith.login.service;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.sql.Timestamp;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.cloudfoundry.identity.uaa.codestore.ExpiringCode;
import org.cloudfoundry.identity.uaa.codestore.ExpiringCodeStore;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hangwith.common.model.BaseMessage;
import com.hangwith.common.model.security.User;

/**
 * Custom reset password controller.
 * Inspired by @see {org.cloudfoundry.identity.uaa.scim.endpoints.PasswordResetEndpoints}
 * We can not use that endpoint b/c:
 * 1) Only reset password either jdbc or in memorty db
 * 2) password* endpoints requires Authorization.
 * 3) We need to send email to the user.
 * @author rshahbazi
 *
 */
@Controller
@RequestMapping("/hw")
public class ResetPasswordController {
	public static final int PASSWORD_RESET_LIFETIME = 30 * 60 * 1000;

	@Autowired
	MongoTemplate mongoTemplate;
	
	//private ScimUserProvisioning scimUserProvisioning;
	@Autowired
    ExpiringCodeStore expiringCodeStore;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	
	@Autowired
	LoginUserGateway loginUserGateway;
	
	@RequestMapping(value = "/password_choose", method = RequestMethod.GET)
    public String resetPasswordPage(PasswordChange passwordChange, ModelMap map) {
		if (StringUtils.isBlank(passwordChange.getCode()) ) {
            map.addAttribute("error","Reset password code is missing" );
            return "reset_password";
        }
		// We can not use retrieveCode since it removes the code!
		
		/*ExpiringCode expiringCode = expiringCodeStore.retrieveCode(passwordChange.getCode());
        if (expiringCode == null) {
            map.addAttribute("error","Reset password token is not found or expired:" +passwordChange.getCode());
            return "reset_password";
        }
        if (passwordChange.getCode().equalsIgnoreCase(passwordChange.getEmail())) {
            map.addAttribute("error","Reset password code does not much with your email address" );
            return "reset_password";
        }
        
        String userId = expiringCode.getData();	        
        User user = mongoTemplate.findById(userId,  User.class);*/
        map.addAttribute("code",passwordChange.getCode());
        map.addAttribute("email",passwordChange.getEmail());
		return "reset_password";
	}
	
	
	@RequestMapping(value = "/password_resets", method = RequestMethod.GET)
    public @ResponseBody BaseMessage resetPassword(String email) {
		User user = mongoTemplate.findOne(query(where("email").is(email.toLowerCase(Locale.US))), User.class);        
        if (user==null) {
        	return new BaseMessage(404,"client.resourceNotFound","User not found for email:" +email);
        }
        if (user.getActive()!=null && !user.getActive()){
        	return new BaseMessage(401,"login.accountNotActive","Account is not active:" +email);
        }
        if (StringUtils.isBlank(user.getEmail()) ){
        	return new BaseMessage(404,"client.resourceNotFound","User's email is missing, no way to recover this account!:" +user.getObjectId());
        }
        
        String  code = expiringCodeStore.generateCode(user.getObjectId(), new Timestamp(System.currentTimeMillis() + PASSWORD_RESET_LIFETIME)).getCode();
        user.setPassword(null);
        user.setOperation(code);
        loginUserGateway.resetPassword(user);
        return new BaseMessage("Password reset requested",email);
    }
	
	@RequestMapping(value = "/password_change_action", method = RequestMethod.POST)
    public String changePasswordPage( PasswordChange passwordChange) {
		BaseMessage result = changePassword(passwordChange);
		
		return "redirect:password_change_success?code="+result.getCode();
	}
	@RequestMapping(value = "/password_reset", method = RequestMethod.GET)
    public String passwordResetPage( PasswordChange passwordChange, ModelMap map) {
		map.addAttribute("passwordChange",passwordChange);
		return "reset_password_request";
	}
	@RequestMapping(value = "/password_change_success", method = RequestMethod.GET)
    public String changePasswordPage( String code, ModelMap map) {
		map.addAttribute("code", code);
		return "reset_password_success";
	}
	@RequestMapping(value = "/password_change", method = RequestMethod.POST)
    public @ResponseBody BaseMessage changePassword(@RequestBody PasswordChange passwordChange) {
		BaseMessage responseEntity;
        if (isCodeAuthenticatedChange(passwordChange)) {
            responseEntity = changePasswordCodeAuthenticated(passwordChange);
        } else if (isUsernamePasswordAuthenticatedChange(passwordChange)) {
            responseEntity = changePasswordUsernamePasswordAuthenticated(passwordChange);
        } else {
            responseEntity = new BaseMessage(400, "bad request");
        }
        return responseEntity;
    }
	
	private boolean isCodeAuthenticatedChange(PasswordChange passwordChange) {
        return passwordChange.getCode() != null && passwordChange.getCurrentPassword() == null && passwordChange.getUsername() == null;
    }
	 private boolean isUsernamePasswordAuthenticatedChange(PasswordChange passwordChange) {
	        return passwordChange.getUsername() != null && passwordChange.getCurrentPassword() != null && passwordChange.getCode() == null;
	}								
	 private BaseMessage changePasswordCodeAuthenticated(PasswordChange passwordChange) {
	        ExpiringCode expiringCode = expiringCodeStore.retrieveCode(passwordChange.getCode());
	        if (expiringCode == null) {
	            return new BaseMessage(400,"login.tokenExpired","Reset password token is not found or expired:" +passwordChange.getCode());
	        }
	        String userId = expiringCode.getData();	        
	        User db = mongoTemplate.findById(userId,  User.class);
	        try {
	        	db.setBcryptPassword(passwordEncoder.encode(passwordChange.getNewPassword()));
	        	db.setUpdatedAt(new DateTime(DateTimeZone.UTC));
	        	mongoTemplate.save(db);
	            return new BaseMessage("Successufully changed the password",db.getUsername());
	        } catch (BadCredentialsException x) {
	        	return new BaseMessage(403,"user.accessDenied",x.getLocalizedMessage());
	           
	        } catch (Exception x) {
	        	return new BaseMessage(500,"server.error",x.getLocalizedMessage());
	            
	        }
	    }
	 private BaseMessage changePasswordUsernamePasswordAuthenticated(PasswordChange passwordChange) {
	        
	        User user = mongoTemplate.findById(passwordChange.getUsername(), User.class);      
	        if (user==null) {
	        	return new BaseMessage(404,"client.resourceNotFound","User not found:" +passwordChange.getUsername());
	        }
	        String oldPassword = passwordChange.getCurrentPassword();
	       
	        try {
	        	user.setBcryptPassword(passwordEncoder.encode(passwordChange.getNewPassword()));
	        	user.setUpdatedAt(new DateTime(DateTimeZone.UTC));
	        	mongoTemplate.save(user);
	        	return new BaseMessage("Successufully changed the password",user.getUsername());
	        } catch (BadCredentialsException x) {
	         
	        	return new BaseMessage(403,"user.accessDenied",x.getLocalizedMessage());
	        }catch (Exception x) {	         
	        	return new BaseMessage(500,"server.error",x.getLocalizedMessage());
	        }
	    }
}
