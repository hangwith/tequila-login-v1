package com.hangwith.login.service;

import org.springframework.integration.annotation.Gateway;
import org.springframework.stereotype.Component;

import com.hangwith.common.model.security.User;

/**
 * Downstream processes and Events related to the user and login , such as reset password email.
 * @author rshahbazi
 *
 */
@Component
public interface LoginUserGateway {
	@Gateway(requestChannel="resetPasswordChannel")
    public void resetPassword(User user);
}
