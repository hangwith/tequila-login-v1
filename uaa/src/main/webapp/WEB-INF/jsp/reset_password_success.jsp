<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <!-- This page is displayed whenever someone has successfully reset their password.
       Pro and Enterprise accounts may edit this page and tell Parse to use that custom
       version in their Parse app. See the App Settigns page for more information.
       This page will be called with the query param 'username'
    -->
  <head>
  <title>Password Reset</title>
  <style type='text/css'>
    h1 {
      color: #0067AB;
      display: block;
      font: inherit;
      font-family: 'Open Sans', 'Helvetica Neue', Helvetica;
      font-size: 30px;
      font-weight: 600;
      height: 30px;
      line-height: 30px;
      margin: 45px 0px 0px 45px;
      padding: 0px 8px 0px 8px;
    }
    .error {
		color: red;
		
	}
  </style>
  <body>
    <c:if test="${code eq 200}">
    	<h1>Successfully updated your password!</h1>
    </c:if>
    <c:if test="${code ne 200}">
    	<h1 class="error">Unable to change the password!</h1>
    </c:if>
    
  </body>
</html>
